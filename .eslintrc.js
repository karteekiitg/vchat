module.exports = {
    "parser": "babel-eslint",
    "extends": [
        "airbnb-base",
        "plugin:react/recommended",
    ],
    "rules": {
        // enable additional rules
        "linebreak-style": ["error", "windows"],
        "react/jsx-uses-react": "error",
        "react/jsx-uses-vars": "error",
    },
    "plugins": [
        "react"
    ],
    "env": {
      "browser": true,
      "node": true
    }
};