let express = require('express');
let app = express();
let admin = require('firebase-admin');

let serviceAccount = require('./vchat-7c774-firebase-adminsdk-np923-40843bc26d.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://vchat-7c774.firebaseio.com'
});

app.get('/', function (req, res) {
  res.send('Hello World');
});

app.get('/chat/check_password', async function (req, res) {
  const user = req.query.user;
  const server = req.query.server;
  const pass = req.query.pass;
  console.log('check_password', user, server, pass);
  if (server.toLowerCase() != 'mbhospitals.com') {
    console.log('Wrong server', server);
    res.send(false);
  } else {
    try {
      let decodedToken = await admin.auth().verifyIdToken(pass);
      let uid = decodedToken.uid;
      if (uid.toLowerCase() != user.toLowerCase()) {
        console.log('Error in user', user, uid, decodedToken);
        res.send(false);
      } else {
        console.log('Auth success', user, server);
        res.send(true);
      }
    } catch (e) {
      console.log('Error in decoding');
      res.send(false);
    }
  }
});

app.get('/chat/user_exists', function (req, res) {
  const user = req.query.user;
  const server = req.query.server;
  const pass = req.query.pass;
  console.log('user_exists', user, server, pass);
  res.send(true);
});

app.listen(8000, function () {
    console.log('Example app listening on port 8000.');
});
