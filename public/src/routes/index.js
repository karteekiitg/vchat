import React from 'react';
import { Route, Switch } from 'react-router';
import Home from '../components/Home';
import Chat from '../components/Chat';

const routes = (
  <div>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/chat" component={Chat} />
    </Switch>
  </div>
);

export default routes;
