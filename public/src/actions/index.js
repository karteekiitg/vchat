import {
  REGISTER_FIREBASE_AUTH_LISTENER, UNREGISTER_FIREBASE_AUTH_LISTENER,
  AUTH_STATE_CHANGED_LOGGEDIN, AUTH_STATE_CHANGED_LOGGEDOUT,
  POST_CHAT, PARTNER_JOIN, RECEIVE_CHAT, LOAD_CHATS, LOAD_CHATS_ACTION,
  REQUEST_PUSH_PERMISSION, REGISTER_SERVICE_WORKER,
} from '../constants';

export const registerFirebaseAuthListener = () => ({ type: REGISTER_FIREBASE_AUTH_LISTENER });
export const unregisterFirebaseAuthListener = () => ({ type: UNREGISTER_FIREBASE_AUTH_LISTENER });
export const requestPushPermission = () => ({ type: REQUEST_PUSH_PERMISSION });
export const registerServiceWorker = () => ({ type: REGISTER_SERVICE_WORKER });

export const authStateChangedLoggedIn = profile => ({
  type: AUTH_STATE_CHANGED_LOGGEDIN,
  payload: { profile, signInState: true },
});

export const authStateChangedLoggedOut = () => ({
  type: AUTH_STATE_CHANGED_LOGGEDOUT,
  payload: { profile: null, signInState: false },
});

export const loadChats = uid => ({ type: LOAD_CHATS, payload: { uid } });
export const loadChatsAction = chats => ({ type: LOAD_CHATS_ACTION, payload: { chats } });
export const postChat = (from, to, msg) => ({ type: POST_CHAT, payload: { msg, from, to } });
export const receiveChat = msg => ({ type: RECEIVE_CHAT, payload: { msg } });

export const partnerJoin = partnerId => ({
  type: PARTNER_JOIN,
  payload: { partnerId },
});

export const updatePartnerConnId = partnerJoin;
