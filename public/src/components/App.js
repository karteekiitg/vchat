import { hot } from 'react-hot-loader';
import React from 'react';
import { ConnectedRouter } from 'connected-react-router';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { StyledFirebaseAuth } from 'react-firebaseui';

import routes from '../routes';
import { registerFirebaseAuthListener, unregisterFirebaseAuthListener, registerServiceWorker } from '../actions';

class App extends React.Component {
  componentDidMount() {
    this.props.registerFirebaseAuthListener();
    this.props.registerServiceWorker();
  }

  componentWillUnmount() {
    this.props.unregisterFirebaseAuthListener();
  }

  render() {
    console.log('Apppppp proppppppssss:', this.props.signInState, this.props.firebase);
    if (this.props.signInState === null || this.props.signInState === undefined) {
      return (
          <div>
            Wait
          </div>
      );
    }
    if (!this.props.signInState) {
      const firebaseUiConfig = {
        signInFlow: 'redirect',
        signInOptions: [
          this.props.firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        ],
      };
      return (
          <div>
            <h1>My App</h1>
            <p>Please sign-in:</p>
            <StyledFirebaseAuth uiConfig={firebaseUiConfig} firebaseAuth={this.props.firebase.auth()} />
          </div>
      );
    }
    return (
        <div>
          <h1>My App</h1>
          <p>Welcome! You are now signed-in!</p>
          <ConnectedRouter history={this.props.history}>
            { routes }
          </ConnectedRouter>
        </div>
    );
  }
}

const mapStateToProps = state => ({
  profile: state.authReducer.profile,
  signInState: state.authReducer.signInState,
  firebase: state.authReducer.firebase,
});

export default hot(module)(compose(
  connect(mapStateToProps, { registerFirebaseAuthListener, unregisterFirebaseAuthListener, registerServiceWorker }),
)(App));
