import React from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import { compose } from 'redux';
import { connect } from 'react-redux';

class Home extends React.Component {
  onSignOut() {
    this.props.firebase.auth().signOut();
  }

  render() {
    return(
      <React.Fragment>
          <h1>Hello Home</h1>
          <Link to={'/chat'}>Chat</Link>
          <br/>
          <Link to={`/chat?partnerId=${this.props.id}`}>Chat Invite</Link>
          <br/>
          <Button variant="outlined" color="secondary" onClick={() => this.onSignOut()}>
            SignOut
          </Button>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  firebase: state.authReducer.firebase,
  id: state.authReducer.profile.uid,
});

export default compose(
  connect(mapStateToProps),
)(Home);
