import React from 'react';
import { Link } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import { compose } from 'redux';
import { connect } from 'react-redux';
import queryString from 'query-string';

import { postChat, partnerJoin, loadChats } from '../actions';

class Chat extends React.Component {
    state = {
      value: '',
    }

    componentDidMount() {
      const queryProps = queryString.parse(this.props.location.search, { ignoreQueryPrefix: true });
      console.log('queryProps::::::22222', queryProps);
      if ('partnerId' in queryProps) {
        console.log('queryProps::::::', queryProps);
        this.props.partnerJoin(queryProps.partnerId);
      }
      this.props.loadChats(this.props.uid);
    }

    handleChange(o) {
      console.log(o.target.value);
      this.setState({ value: o.target.value });
    }

    handleEnterKey(o) {
      if (o.key === 'Enter' && this.state.value) {
        const value = '';
        this.setState({ value });
        this.props.postChat(this.props.uid, this.props.partnerId, this.state.value);
      }
    }

    onPostChat() {
      if (this.state.value) {
        const value = '';
        this.setState({ value });
        this.props.postChat(this.props.uid, this.props.partnerId, this.state.value);
      }
    }

    render() {
      console.log('***********', this.props);
      const chats = this.props.chats.slice(0).reverse().map((chat, index) => <li key={index}>{chat.msg}:{chat.status}</li>);
      console.log('***********2222222222', this.props.chats);
      return (
        <React.Fragment>
          <h1>Hello Chat</h1>
          <Link to={'/'}>Home</Link>
          <TextField
            id="outlined-password-input"
            value={this.state.value}
            label="Chat"
            fullWidth
            margin="normal"
            variant="outlined"
            onChange={o => this.handleChange(o)}
            onKeyUp={o => this.handleEnterKey(o)}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton aria-label="Toggle password visibility">
                    <Icon color="error" style={{ fontSize: 30 }} onClick={() => this.onPostChat()}>
                      add_circle
                    </Icon>
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
          <ul>
          {chats}
          </ul>
        </React.Fragment>
      );
    }
}

const mapStateToProps = state => ({
  chats: state.chatReducer.chats,
  uid: state.authReducer.profile.uid,
  partnerId: state.authReducer.profile.partnerId,
});

export default compose(
  connect(mapStateToProps, { postChat, partnerJoin, loadChats }),
)(Chat);
