import {
  take, put, call, fork, all, takeEvery, select, actionChannel,
} from 'redux-saga/effects';
import uuid from 'uuid';
import firebase from 'firebase';
import PouchDB from 'pouchdb';
import PouchDBUpsert from 'pouchdb-upsert';
import PouchdbFind from 'pouchdb-find';

import {
  REGISTER_FIREBASE_AUTH_LISTENER, UNREGISTER_FIREBASE_AUTH_LISTENER,
  AUTH_STATE_CHANGED_LOGGEDIN, POST_CHAT, PARTNER_JOIN, LOAD_CHATS,
  REQUEST_PUSH_PERMISSION, REGISTER_SERVICE_WORKER,
} from '../constants';
import AuthChannel from '../channels/auth';
import wschannel from '../channels/ws';
import pushchannel from '../channels/push';

import {
  authStateChangedLoggedIn, authStateChangedLoggedOut,
  receiveChat as receiveChatAction, loadChatsAction,
  requestPushPermission as requestPushPermissionAction,
} from '../actions';

const getUid = state => (state.authReducer.profile
  ? state.authReducer.profile.uid
  : undefined);
const getProfile = state => state.authReducer.profile;

PouchDB.plugin(PouchdbFind);
PouchDB.plugin(PouchDBUpsert);
const db = new PouchDB('chats');

console.log(db);
db.createIndex({
  index: {
    fields: ['from'],
  },
});

db.createIndex({
  index: {
    fields: ['to'],
  },
});

db.createIndex({
  index: {
    fields: ['msgtype'],
  },
});

function* getInitialMsgs(profile) {
  if (profile.partnerId) {
    const connId = profile.uid < profile.partnerId
      ? `${profile.uid}:${profile.partnerId}`
      : `${profile.partnerId}:${profile.uid}`;
    const msg = {
      msgtype: 'iims',
      connId,
    };
    try {
      const doc = yield db.get('last_msg');
      msg.timestamp = doc.timestamp;
      wschannel.json(msg);
    } catch (e) {
      /* eslint-disable no-empty */
    }
  }
}

function* firebaseAuthListener() {
  yield take(REGISTER_FIREBASE_AUTH_LISTENER);
  const channel = yield call(AuthChannel.getInstance);
  while (true) {
    try {
      const payload = yield take(channel);
      if (payload.user) {
        const profile = {
          msgtype: 'sp',
          email: payload.user.email,
          displayName: payload.user.providerData[0].displayName || payload.user.email,
          avatarUrl: payload.user.providerData[0].photoURL,
          uid: payload.user.uid,
        };

        console.log('4444444444#############');
        let docProfile;
        yield db.upsert('profile', (doc) => {
          /* eslint-disable no-underscore-dangle */
          if (doc._id) {
            docProfile = { ...doc, _id: 'profile', deviceId: (doc && doc.deviceId) ? doc.deviceId : uuid.v1() };
          } else {
            docProfile = { ...profile, deviceId: uuid.v1() };
          }
          return docProfile;
        });
        yield db.upsert('device', (doc) => {
          /* eslint-disable no-underscore-dangle */
          if (doc._id) {
            return { ...doc, _id: 'device', deviceId: (doc && doc.deviceId) ? doc.deviceId : uuid.v1() };
          }
          return { ...profile, _id: 'device', deviceId: uuid.v1() };
        });
        console.log('4444444444#############', docProfile);
        const newProfile = { ...docProfile, ...profile };
        yield put(authStateChangedLoggedIn(newProfile));
      } else {
        yield put(authStateChangedLoggedOut());
      }
    } catch (e) {
      console.log('channel error for auth', e);
    }
  }
}

function* unregisterFirebaseAuthListener() {
  yield take(UNREGISTER_FIREBASE_AUTH_LISTENER);
  const channel = yield call(AuthChannel.getInstance);
  channel.close();
}

function* handlePostChat(action) {
  console.log(action.payload.to, action.payload.msg);
  const uid = yield select(getUid);
  if (action.payload.to && action.payload.msg) {
    const msg = {
      from: uid,
      to: `${action.payload.to}`,
      msg: action.payload.msg,
      msgtype: 'im',
      _id: uuid.v1(),
      status: 0,
      timestamp: (new Date()).getTime(),
    };
    yield put(receiveChatAction(msg));
    yield db.put(msg);
    msg.status = 1;
    wschannel.json(msg);
  }
}

function* postChat() {
  yield takeEvery(POST_CHAT, handlePostChat);
}

function* upsertChat(msg) {
  /* eslint-disable no-underscore-dangle */
  console.log('(((((((((((((((((((', msg);
  let docMsg;
  yield db.upsert(msg._id, (doc) => {
    /* eslint-disable no-underscore-dangle */
    if (doc._id) {
      if (msg.status > doc.status) {
        docMsg = { ...doc, status: msg.status };
        return docMsg;
      }
      docMsg = doc;
      return false;
    }
    // Read receipts
    // wschannel.json(msg);
    docMsg = { ...msg, status: 2 };
    return docMsg;
  });
  console.log('(((((((((((((((((((', docMsg);
  yield db.upsert('last_msg', () => ({ timestamp: docMsg.timestamp }));
  return docMsg;
}

function* handleChat(msg) {
  const profile = yield select(getProfile);
  console.log('*****************', msg, msg.msgtype);
  if (msg.msgtype === 'im') {
    const message = yield upsertChat(msg);
    yield put(receiveChatAction(message));
  } else if (msg.msgtype === 'gp') {
    console.log('1+1+++++++++++++++++++++++++++', msg);
    let docProfile;
    yield db.upsert('profile', () => {
      docProfile = { ...profile, ...msg, _id: 'profile' };
      return docProfile;
    });
    yield put(authStateChangedLoggedIn(docProfile));
    yield getInitialMsgs(profile);
  }
}

function* receiveChat() {
  yield take(AUTH_STATE_CHANGED_LOGGEDIN);
  const token = yield firebase.auth().currentUser.getIdToken(true);
  const profile = yield select(getProfile);
  let device;
  try {
    device = yield db.get('device');
  } catch (e) {
    device = { deviceId: uuid.v1() };
  }
  const channel = yield call(wschannel.getInstance, profile, token, device);
  while (true) {
    try {
      console.log(1000000000000000000);
      const msg = yield take(channel);
      console.log(111111111111111111);
      yield handleChat(msg);
    } catch (e) {
      console.log('channel error for chats', e);
    }
  }
}

function* partnerJoin() {
  const action = yield take(PARTNER_JOIN);
  const profile = yield select(getProfile);
  console.log('@@@@@@@@@@@@@@@@@@@@@@', profile);
  profile.msgtype = 'sp';
  profile.partnerId = action.payload.partnerId;
  console.log('@@@@@@@@@@@@@@@@@@@@@@', profile);
  wschannel.json(profile);
}

function* loadChats(action) {
  const resultFrom = yield db.find({
    selector: { from: action.payload.uid, msgtype: 'im' },
  });
  const resultTo = yield db.find({
    selector: { to: action.payload.uid, msgtype: 'im' },
  });
  const chats = [...resultFrom.docs, ...resultTo.docs];
  console.log('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^', action, chats);
  yield put(loadChatsAction(chats));
}

function* loadChatsWrapper() {
  yield takeEvery(LOAD_CHATS, loadChats);
}

function* requestPushPermission() {
  yield take(REQUEST_PUSH_PERMISSION);
  console.log(111111111111);
  try {
    const messaging = firebase.messaging();
    console.log(44444444444);
    const permission = yield messaging.requestPermission();
    console.log(3333333333, permission);
    const pushToken = yield messaging.getToken();
    console.log(222222222222, pushToken);
    yield db.upsert('device', (doc) => {
      /* eslint-disable no-underscore-dangle */
      if (doc._id) {
        return { ...doc, _id: 'device', pushToken };
      }
      return { _id: 'device', pushToken };
    });
  } catch (err) {
    // eslint-disable no-empty
    console.log('errrrrrrrrrrrrrrrrrrrr', err);
  }
}

function* tokenRefresh() {
  yield take(REQUEST_PUSH_PERMISSION);
  const channel = yield call(pushchannel.getInstance);
  console.log(5555555555555);
  while (true) {
    try {
      console.log(6666666666);
      const pushToken = yield take(channel);
      console.log(777777777777, pushToken);
      yield db.upsert('device', (doc) => {
        /* eslint-disable no-underscore-dangle */
        if (doc._id) {
          return { ...doc, _id: 'device', pushToken };
        }
        return { _id: 'device', pushToken };
      });
    } catch (e) {
      console.log('channel error for chats', e);
    }
  }
}

function* registerServiceWorker() {
  yield take(REGISTER_SERVICE_WORKER);
  if ('serviceWorker' in navigator) {
    try {
      const registration = yield navigator.serviceWorker.register('/sw.js');
      registration.pushManager.subscribe({ userVisibleOnly: true });
      console.log('SW registered: ', registration);
      const fbRegistration = yield navigator.serviceWorker.register('/firebase-messaging-sw.js');
      fbRegistration.pushManager.subscribe({ userVisibleOnly: true });
      console.log('SW registered: ', fbRegistration);
    } catch (registrationError) {
      console.log('SW registration failed: ', registrationError);
    }
  }
  yield put(requestPushPermissionAction());
}

export default function* rootSaga() {
  yield all([
    fork(receiveChat),
    fork(firebaseAuthListener),
    fork(unregisterFirebaseAuthListener),
    fork(postChat),
    fork(partnerJoin),
    fork(loadChatsWrapper),
    fork(requestPushPermission),
    fork(tokenRefresh),
    fork(registerServiceWorker),
  ]);
}
