import { createBrowserHistory } from 'history';
import { compose, createStore, applyMiddleware } from 'redux';
import { routerMiddleware, connectRouter } from 'connected-react-router';
import createSagaMiddleware from 'redux-saga';
import firebase from 'firebase';

import rootReducer from './reducers';
import rootSaga from './sagas';
import firebaseConfig from './config/firebase.config';

// Initialize Firebase instance
firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();
messaging.usePublicVapidKey('BBhh0RBOz9I4Q0kM4M1EWB0d8EHxU00BBO_PtIaq7h9wufojxwciFZIn64rOXJ94frxYA2mvfZDKD2zRpMPlbMM');

// History
const history = createBrowserHistory();

const sagaMiddleware = createSagaMiddleware();

// Connect history and root reducer
const newRootReducer = connectRouter(history)(rootReducer);
const middleware = [
  sagaMiddleware,
  routerMiddleware(history),
];

/* eslint no-underscore-dangle: ["error", { "allow": ["__REDUX_DEVTOOLS_EXTENSION_COMPOSE__"] }] */
const composeEnhancers = typeof window === 'object'
 && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
  }) : compose;

const enhancer = composeEnhancers(
  applyMiddleware(...middleware),
);

// Create store with reducers and initial state
const store = createStore(newRootReducer, {}, enhancer);
sagaMiddleware.run(rootSaga);

export { store, history };
