import firebase from 'firebase';
import { eventChannel, buffers } from 'redux-saga';

class AuthChannel {
  constructor() {
    if (!AuthChannel.instance) {
      this.authChannel = null;
      AuthChannel.instance = this;
    }
    return AuthChannel.instance;
  }

  getInstance() {
    console.log(this);
    const self = AuthChannel.instance;

    if (!self.authChannel) {
      self.authChannel = eventChannel((emit) => {
        const unsubscribe = firebase.auth().onAuthStateChanged(
          user => emit(user ? { user } : { user: null }),
        );
        return unsubscribe;
      }, buffers.expanding(20));
    }
    return self.authChannel;
  }
}

const instance = new AuthChannel();

export default instance;
