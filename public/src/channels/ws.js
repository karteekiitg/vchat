import { eventChannel, buffers } from 'redux-saga';
import Sockette from 'sockette';
import uuid from 'uuid';

class WSChannel {
  constructor() {
    if (!WSChannel.instance) {
      this.client = null;
      this.eventChannel = null;
      this.pendingMessages = [];
      this.onReady = false;
      this.deviceId = null;
      this.pushToken = null;
      WSChannel.instance = this;
    }
    console.log('^^^^^^^^^^^^^99999999999999999999', WSChannel.instance, this);
    return WSChannel.instance;
  }

  getClient() {
    console.log('^^^^^^^^^^^^^222222', WSChannel.instance, this);
    return WSChannel.instance.client;
  }

  json(msg) {
    const self = WSChannel.instance;
    console.log('^^^^^^^^^^^^^3222222', self, this, self.deviceId);
    if (self.onReady) {
      const msgWithDeviceId = { ...msg, deviceId: self.deviceId };
      self.client.json(msgWithDeviceId);
    } else {
      self.pendingMessages.push(msg);
    }
  }

  getInstance(profile, token, device) {
    console.log('^^^^^^^^^^^^^', profile, token, WSChannel.instance, this, '5555555');
    const url = 'ws://127.0.0.1:8001/chat';
    const self = WSChannel.instance;
    self.deviceId = device.deviceId;
    self.pushToken = device.pushToken;

    if (!self.eventChannel) {
      console.log('^^^^^^^^^^^^^3333333333333333333333333333333333333');
      let sendIntialMessages = true;
      self.eventChannel = eventChannel((emit) => {
        if (!self.client) {
          self.client = new Sockette(url, {
            timeout: 10000,
            onopen: () => {
              let pendingMessages = [];
              if (self.pendingMessages.length !== 0) {
                pendingMessages = self.pendingMessages.slice();
                self.pendingMessages = [];
              }
              self.onReady = true;
              const p = {
                msgtype: 'auth',
                uid: profile.uid,
                token,
              };
              self.json(p);
              console.log('2222222222#############');
              if (sendIntialMessages) {
                sendIntialMessages = false;
                self.json(profile);
                if (self.pushToken) {
                  self.json({
                    msgtype: 'pt',
                    pushToken: self.pushToken,
                    uid: profile.uid,
                  });
                }
              }
              console.log('33333333#############');
              /* eslint-disable no-restricted-syntax */
              for (const msg of pendingMessages) {
                self.json(msg);
              }
              console.log('44444444#############');
            },
            onmessage: (msg) => {
              console.log('#############++', new Date(), msg);
              emit(JSON.parse(msg.data));
            },
            onclose: () => {
              self.onReady = false;
              console.log('%%%%%%%%%%%%%%%', new Date(), self.client);
              // Reconnect with 0-20 sec so as not to overload server
            },
          });
        }
        console.log(self.client);
        return () => {};
      }, buffers.expanding(200));
    }
    return self.eventChannel;
  }
}

const instance = new WSChannel();

export default instance;
