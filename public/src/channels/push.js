import firebase from 'firebase';
import { eventChannel, buffers } from 'redux-saga';

class PushChannel {
  constructor() {
    if (!PushChannel.instance) {
      this.pushChannel = null;
      PushChannel.instance = this;
    }
    return PushChannel.instance;
  }

  getInstance() {
    console.log(this);
    const self = PushChannel.instance;

    if (!self.pushChannel) {
      self.pushChannel = eventChannel((emit) => {
        const messaging = firebase.messaging();
        const unsubscribe = messaging.onTokenRefresh(function* tokenRefresh() {
          try {
            const pushToken = yield messaging.getToken();
            console.log('Token refresh::::::::::::::::', pushToken);
            emit(pushToken);
          } catch (err) {
            // eslint-disable no-empty
          }
        });
        return unsubscribe;
      }, buffers.expanding(20));
    }
    return () => {};
  }
}

const instance = new PushChannel();

export default instance;
