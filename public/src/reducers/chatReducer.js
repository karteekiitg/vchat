import { RECEIVE_CHAT, LOAD_CHATS_ACTION } from '../constants';

const defaultState = { chats: [] };

const chatReducer = (state = defaultState, action) => {
  let chats;
  switch (action.type) {
    case RECEIVE_CHAT:
      console.log(state);
      chats = [];
      /* eslint-disable no-restricted-syntax */
      for (const chat of state.chats) {
        /* eslint-disable no-underscore-dangle */
        if (!(chat && chat._id === action.payload.msg._id)) {
          chats.push(chat);
        }
      }
      chats.push(action.payload.msg);
      console.log('stateeeeeeeeeeeeeeeeeeeeeee:', chats);
      return { ...state, chats };
    case LOAD_CHATS_ACTION:
      console.log(state);
      chats = [...action.payload.chats, ...state.chats];
      console.log('stateeeeeeeeeeeeeeeeeeeeeee22222222:', chats);
      return { ...state, chats };
    default:
      return state;
  }
};

export default chatReducer;
