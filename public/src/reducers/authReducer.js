import firebase from 'firebase';

import {
  AUTH_STATE_CHANGED_LOGGEDIN, AUTH_STATE_CHANGED_LOGGEDOUT, PARTNER_JOIN,
} from '../constants';

const defaultState = {
  firebase, signInState: null, profile: null,
};

const authReducer = (state = defaultState, action) => {
  switch (action.type) {
    case AUTH_STATE_CHANGED_LOGGEDIN:
    case AUTH_STATE_CHANGED_LOGGEDOUT:
      console.log('&&&&&&&&&&&&&&&&&&:', action.payload, { ...state, ...action.payload });
      return { ...state, ...action.payload };
    case PARTNER_JOIN:
      /* eslint-disable no-case-declarations */
      const profile = JSON.parse(JSON.stringify(state.profile));
      profile.partnerId = action.payload.partnerId;
      return { ...state, profile };
    default:
      return state;
  }
};

export default authReducer;
