import 'regenerator-runtime/runtime';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/App';
import { store, history } from './store';

const render = () => {
  ReactDOM.render(
        <Provider store={store}>
          <App history={history} />
        </Provider>,
        document.getElementById('container'),
  );
};

render();
