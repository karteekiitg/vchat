importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');

firebase.initializeApp({
  messagingSenderId: '626577548385',
});

const messaging = firebase.messaging();

self.addEventListener('install', function(event) {
  console.log('Service Worker installing.', messaging);
});

self.addEventListener('activate', function(event) {
  console.log('Service Worker activating.', messaging);
});

pyth(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  var notificationTitle = 'Background Message Title';
  var notificationOptions = {
    body: 'Background Message body.',
  };

  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  return self.registration.showNotification(notificationTitle, notificationOptions);
});

self.addEventListener('push', function(event) {
  console.log('[Service Worker] Push Received.', event.data.text());
  // show
  event.waitUntil(self.registration.showNotification("data.title", {
    body: 'Background Message body.',
    tag: "123",
  }));
});
